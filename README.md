# SAE-WEB 
## Index.html

#### La page d'accueil du site :   

Contient une barre de navigation avec des liens vers Accueil, Abonnement, et Distance.
Présente des sections sur les différents moyens de transport à Lyon, notamment Vélo'v, Tramways, Trains, Marche, et Métros.
Comprend un formulaire de contact à la fin de la page en cas de questions de l'utilisateur.
Le style visuel est basé sur des couleurs distinctes pour chaque section, avec des images représentatives.

## abonnement.html
#### La page dédiée aux abonnements pour se rendre à l'IUT.
Comprend également une barre de navigation similaire à la page d'accueil.
Présente des sections sur les abonnements Vélo'v, TCL, et SNCF, avec des boutons pour acceder au site internet des entreprises pour leurs abonnnements.
Affiche un tableau comparatif des prix des différents abonnements.

## distance.html
#### La page qui informe sur le temps de trajet en fonction de la distance du lieu d'habitation.
Comprend une barre de navigation similaire aux autres pages.
Présente des sections sur la distance dans Villeurbanne, Lyon, la périphérie, et la région, avec des informations sur le temps de trajet.
Affiche une carte légendé représentant différentes zones.

## Style (style.css)
Utilise des couleurs différentes pour chaque catégorie de contenu (Bleu, Or, Violet).
Voici les différentes couleurs :
- `#0073c4` Pour la page d'accueil
- `#CBA834` Pour la page abonnement
- `#7B0DC3` Pour la page distance 
- `#0f0f0f` Pour la couleur du texte dans les pages

Le fond de la page reprends la couleur de la page actuelle et forme des petits rectangle en ajoutant de la transparence sur celle là, ces formes permettent à cette page une qualité esthétique amélioré. 
- `rgba(0, 115, 196, 0.34)` Pour la page d'accueil
- `rgba(203, 168, 52, 0.34)` Pour la page abonnement
- `rgba(123, 13, 195, 0.34)` Pour la page distance 

Utilise des classes spécifiques pour le style de la barre de navigation, des sections, des boutons, et d'autres éléments.
La mise en page est responsive avec des ajustements pour différentes tailles d'écrans.

La majorité du positionnement des éléments est effectué avec un display: flex; et seulement le footer et le formulaire de la page d'accueil utilise des display:grid.